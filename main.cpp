
#include "stack.h"
#include "position.h"
#include "bot.h"

#include <stdio.h>
#include <map>
#include <stdexcept>

using namespace std;

int main()
{
    /*stack<int> spine;
    spine.push(1);
    spine.push(3);
    spine.push(7);
    printf("%d\n", spine.pop());
    int a = spine.pop();
    printf("%d\n",a);
    printf("%d\n", spine.topNode());
    printf("%d\n", spine.pop());

    try
    {
        printf("%d\n", spine.pop());
    }
    catch (const exception& e)
    {
        cerr<<e.what();
        exit(EXIT_FAILURE);
    }*/


    /*position<int> p1(10,200);
    position<int> p2(20,20);
    position<int> p3(30,-250);
    std::map<const position<int>, int> pointMap;
    pointMap[p1] = 1;
    pointMap[p2] = 2;
    pointMap[p3] = 3;
    pointMap[p2] = 4;

    cout<<p1<<endl;


    printf("%d\n",pointMap[p2]);
    p1 = p2;
    p1 = p2 + p3;

    printf("%d\n",(p<p1));
    printf("%d\n",(p1.getX()));
    printf("%d\n",(p1.getY()));
    */


    /*Env_map<int> envMap("input5x5.csv",5,5);
    envMap.initialize(p);
    for (int i=-1;i<= 5; i++){
        for (int j=-1;j<=5; j++){
            position<int> p11(i,j);
            printf("Obstacle %d at ",envMap.isObstacle(p11));
            cout<<p11<<endl;
        }
    }
    envMap.printMap();*/

    position<int> p(3,4);
    /*position<int> p1(5,6);
    position<int> p2(10,5);*/

    bot<int, position> bot1("test_files/input5x5.csv",5,5,p,"127.0.0.1",20002);
    /*bot1.setMarked(p,0);
    bot1.setMarked(p1,1);
    bot1.setMarked(p2,2);

    cout<<p<<endl;
    cout<<"value returned by isPresent : "<< boolalpha <<  bot1.isPresent(p)<<endl;
    cout<<"value returned by isCleaned : "<< boolalpha <<  bot1.isCleaned(p)<<endl;

    cout<<p1<<endl;
    cout<<"value returned by isPresent : "<< boolalpha <<  bot1.isPresent(p1)<<endl;
    cout<<"value returned by isCleaned : "<< boolalpha <<  bot1.isCleaned(p1)<<endl;

    cout<<p2<<endl;
    cout<<"value returned by isPresent : "<< boolalpha <<  bot1.isPresent(p2)<<endl;
    cout<<"value returned by isCleaned : "<< boolalpha <<  bot1.isCleaned(p2)<<endl;*/


    bot1.explore();


}
