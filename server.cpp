#include <iostream>
#include <stdlib.h>
#include "server.h"
//#include "position.h"

using namespace std;

int main(int argc, char *argv[])
{
    if(argc == 2){
        int port = atoi(argv[1]);
        if(port && port>10000 && port<60000){
            position<int> p(0,0);
            server<int,position> server1(port);
            if(DEBUG){
                clog<<"SetMarked_Request = 0,\nSetMarked_Response = 1,\nGetPosition_Request = 2,\nGetPosition_Response = 3,\n"
                      <<"Merge_Request = 4,\nMerge_Response = 5\n\n\n";
            }
            server1.startServer();
            cout<<"success\n";
        }else{
            cout<<"Error : Port number should be only integers & between 10000 & 60000\n";
        }
    }else{
        cout<<"Usage : \nserver <Port number>\n";
    }
    return 0;
}
