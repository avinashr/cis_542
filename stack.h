//========================================================
// stack.h
// Defines class stack
// Author: Avinash Repaka
//========================================================

#pragma once

# include <stdexcept>

using namespace std;

template <class T>
class stack
{
    struct node{
        T data;
        node* next;
        node(T t, node* n) : data(t), next(n) {}
    };
    node* top;

public:
    stack() : top(NULL){}
    void push (T);
    T pop ();
    bool empty() const;
    T topNode() const;
    ~stack();
};


template <class T>
void stack<T>::push (T data){
    node* n = new node(data, top);
    top = n;
}

template <class T>
T stack<T>::pop (void){
    if (empty()){
        throw out_of_range ("ERROR: Trying to pop from an empty stack\n");
    }

    T data(top->data);
    node *temp = top;
    top = top->next;
    delete temp;
    return data;
}

template <class T>
bool stack<T>::empty (void) const{
    return (!top);
}

template <class T>
T stack<T>::topNode (void) const{
    if (empty()){
        throw out_of_range ("ERROR: Trying to read the top element of empty stack\n");
    }
    T data(top->data);
    return data;
}

template <class T>
stack<T>::~stack (){
    node *temp;
    while (top != NULL){
        temp = top;
        top = top-> next;
        delete temp;
    }
}
