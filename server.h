//========================================================
// server.h
// Defines server and functions to handle Bot requests
// Author: Avinash Repaka, Rishabh Gulati
//========================================================


#pragma once

#include <iostream>
#include <map>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <string>
#include <vector>
#include <sstream>
#include "position.h"

using namespace std;

# define DEBUG 1

template <class T, template <class> class N>
class server
{
    map<N<T>, CellState> marked;
    int serverPort;
    recievedMsg deserializeMsgType(string SerializedString)const;
    vector<string> Tokenoize(const string str)const;
    const string delimiter = "..";

public:
    server (int port) : serverPort(port) {}
    void startServer(void);
    string handleResponse(char * buffer,int bufferLength);

};


template <class T, template <class> class N>
void server<T,N>::startServer(void)
{
    int sockfd, newsockfd;
    socklen_t clilen;
    char buffer[256];
    struct sockaddr_in serv_addr, cli_addr;
    int n;

    try{
        sockfd = socket(AF_INET, SOCK_STREAM, 0);
        // set SO_REUSEADDR on a socket to true (1):
        int optval = 1;
        setsockopt(sockfd, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval);

        memset(&serv_addr, 0, sizeof (serv_addr));

        serv_addr.sin_family = AF_INET;
        serv_addr.sin_addr.s_addr = INADDR_ANY;
        serv_addr.sin_port = htons(serverPort);

        bind(sockfd, (struct sockaddr *) &serv_addr,sizeof(serv_addr));

        listen(sockfd,5);

        clilen = sizeof(cli_addr);
        newsockfd = accept(sockfd,
                           (struct sockaddr *) &cli_addr,
                           &clilen);
        while(1){
            bzero(buffer,256);
            n = read(newsockfd,buffer,255);

            buffer[n]=0;

            string toSend = handleResponse(buffer,n);

            n = write(newsockfd,toSend.c_str(),toSend.length());
        }
        close(newsockfd);

        close(sockfd);
    }
    catch (exception& e)
    {
        cerr << "Exception caught in class 'server', function 'startServer' : " << e.what() << '\n';
    }
}

template <class T, template <class> class N>
string server<T,N>::handleResponse(char * buffer,int bufferLength){

    string RecievedSerialisedMsg(buffer,bufferLength);
    recievedMsg recvMsg = deserializeMsgType(RecievedSerialisedMsg);

    if(DEBUG){
        //clog << recvMsg.Msg_Type <<"Message received is : "<< recvMsg.Serialized_payload<<endl;
    }
    switch(recvMsg.Msg_Type){
    case SetMarked_Request : {
        vector<string> strvect = Tokenoize(recvMsg.Serialized_payload);
        if(strvect.size()==3){
            int posX = atoi(strvect[0].c_str());
            int posY = atoi(strvect[1].c_str());
            int markAs = atoi(strvect[2].c_str());
            position<int> position(posX,posY);

            stringstream ss;
            //if(marked.find(position)== marked.end()){
            marked[position] = static_cast<CellState>(markAs);
            ss << SetMarked_Response << delimiter << Marked;
            if(DEBUG){
                clog<<"MARKED";
                clog<<"..........position x : "<<posX<<"..........position y : "<<posY<<endl;
            }
            /*}else{
                ss << SetMarked_Response << delimiter << AlreadyMarked;
                if(DEBUG){
                    clog<<"NOT MARKED"<<endl;
                    clog<<"position x : "<<posX<<"\nposition y : "<<posY<<endl;
                }
            }*/
            return ss.str();
        }
        break;
    }
    case GetPosition_Request : {
        vector<string> strvect = Tokenoize(recvMsg.Serialized_payload);
        if(strvect.size()==2){
            int posX = atoi(strvect[0].c_str());
            int posY = atoi(strvect[1].c_str());
            position<int> position(posX,posY);
            //cout<<"position x : "<<posX<<"..........position y : "<<posY<<endl;
            stringstream ss;
            if(marked.find(position)!= marked.end()){
                ss << GetPosition_Response << delimiter << marked[position];
                if(DEBUG){
                    clog<<"FOUND REQUESTED POSITION IN MAP";
                    clog<<"..........position x : "<<posX<<"..........position y : "<<posY<<endl;
                }
            }else{
                ss << GetPosition_Response << delimiter << NotInMarkedMap;
                if(DEBUG){
                    clog<<"DID NOT FIND REQUESTED POSITION IN MAP";
                    clog<<"..........position x : "<<posX<<"..........position y : "<<posY<<endl;
                }
            }
            return ss.str();

        }
        break;
    }
    case Merge_Request : {
        //TODO
        break;
    }
    default : {
        if(DEBUG){
            clog<<"UNKNOWN MESSAGE RECEIVED"<<endl;
        }
        break;
    }
    }

}

//---------------------------------------
// Returns a structure of type receivedMsg
// for further message processing
//
// Function Name : deserializeMsgType
// Parameters : string SerializedString
// Return Type : recievedMsg(struct)
//---------------------------------------
template <class T, template <class> class N>
recievedMsg server<T,N >::deserializeMsgType(string SerializedString)const
{
    recievedMsg payload;
    vector<string> strvector;
    strvector=Tokenoize(SerializedString);
    vector<string>::iterator itertype=strvector.begin();
    payload.Msg_Type=atoi((const char*)(*itertype).c_str());
    payload.Serialized_payload=SerializedString.substr((*itertype).length()+delimiter.length(),SerializedString.length());
    return payload;
}


//---------------------------------------
// Returns tokenized vector of strings
//
// Function Name : Tokenoize
// Parameters : string str
// Return Type : vector<string>
//---------------------------------------
template <class T, template <class> class N>
vector<string> server<T,N >::Tokenoize(string str)const
{
    vector<string> strvect;
    size_t pos=0, last_pos=0;
    while(last_pos != string::npos)
    {
        last_pos=str.find(delimiter,last_pos+1);
        if(last_pos != string::npos)
            strvect.push_back(str.substr(pos,(last_pos-pos)));
        else
            strvect.push_back(str.substr(pos,(str.size()-pos)));
        pos=delimiter.size()+last_pos;
    }
    return strvect;
}




