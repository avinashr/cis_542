//========================================================
// common_utils.h
// Header file for commons structures/constants/variables
// Author: Rishabh Gulati
//========================================================

#include <string>
// an enum class gives better typechecking
enum Direction{
    North = 0,
    South = 1,
    East = 2,
    West = 3,
    NoOfDirections = 4
};

enum CellState
{
    YetToBeCleaned = 0,
    Cleaned = 1,
    Obstacle = 2    
};

// these are usually not so good. You might do better with a class hierarchy.
// Each message type could then define a virtual function giving what should be done when it is received
enum MessageTypes
{
    SetMarked_Request = 0,
    SetMarked_Response = 1,
    GetPosition_Request = 2,
    GetPosition_Response = 3,
    Merge_Request = 4,
    Merge_Response = 5
};

struct recievedMsg
{
     int Msg_Type;
     std::string Serialized_payload;
};

// these should be class members
/*Constants for GetPosition_Request & GetPosition_Response*/
const int NotInMarkedMap = 3;

/*Constants for SetMarked_Request & SetMarked_Response*/
const int Marked = 1;
const int AlreadyMarked = 0;

