//========================================================
// Env_map.h
// Header and implementation file for generating and
//	 and accessing the environment map for the roomba
// Author: Rishabh Gulati
//========================================================
#pragma once
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <stdint.h>
#include <stdlib.h>
#include <inttypes.h>
#include "position.h"
using namespace std;

// T is conventionally used as the typename of an object being used in a container, not a type of index
template <class T>
class Env_map{

private:
	// in C++ style, you don't use all caps for class constants
	const T ROOM_ROWS;
    const T ROOM_COLS;
    T **mapArray;
    position<T> bot_position;

public:			
	// declare names for your parameters to indicate their purpose, not just their types
	// don't bake the filesystem and its interface into a class that has nothing to do with files
    Env_map(string, T, T);
    ~Env_map();
    void initialize(position<T>);
	void printMap(void);
    position<T> get_position(void);

    /*
    returns a struct of neighbor coordinates
    */
    //neighbors getNeighbors(position<T>);
    bool isObstacle(position<T>);
};

// Don't duplicate in documentation information that is already in the code, 
// e.g. the types of the arguments. Document things like the meaning of the parameters,
// or what the function really does.

//---------------------------------------
// Class constructor
//
// Function Name : Env_map
// Parameters : string filename, T rows, T cols
// Return Type : none
//---------------------------------------.

template <class T>
Env_map<T>::Env_map(string filename, T rows, T cols) : ROOM_ROWS(rows), ROOM_COLS (cols)
{
    mapArray = new T*[ROOM_ROWS];
    for (T i = 0; i < ROOM_ROWS; i++){
        mapArray[i] = new T[ROOM_COLS];
    }

    T row_access = 0;
    T col_access = 0;
    ifstream file(filename.c_str());

    string line;
    while(getline(file,line))
    {
        stringstream lineStream(line);
        string cell;
        col_access=0;
        while(getline(lineStream,cell,','))
        {
            mapArray[row_access][col_access++] = atoi(cell.c_str());
        }
        row_access++;
    }
}

//---------------------------------------
// Class destructor
//
// Function Name : ~Env_map
// Parameters : none
// Return Type : none
//---------------------------------------
template <class T>
Env_map<T>::~Env_map() {
    // De-Allocate so we don't have memory leak
    for (T i = 0; i < ROOM_ROWS; i++)
        delete [] mapArray[i];
    delete [] mapArray;
}

//---------------------------------------
// Print the map
//
// Function Name : printMap
// Parameters : void
// Return Type : void
//---------------------------------------
template <class T>
void Env_map<T>::printMap(void){
    T i,j;
    for(i = 0; i<ROOM_ROWS; i++){
        cout<< "\nRow : "<<i<<endl;
        for(j = 0; j<ROOM_COLS; j++){
            cout<<int(mapArray[i][j])<<" ";
        }
    }
}

//---------------------------------------
// Bot's position intializer
//
// Function Name : initialize
// Parameters : position<T> initial_pos
// Return Type : bool
//---------------------------------------
template <class T>
void Env_map<T>::initialize(position<T> initial_pos){
    bot_position = initial_pos;
}

//---------------------------------------
// Returns the initialized bot's position
//
// Function Name : get_position
// Parameters : void
// Return Type : position<T>
//---------------------------------------
template <class T>
position<T> Env_map<T>::get_position(void){
    return bot_position;
}

//---------------------------------------
// Returns the initialized bot's position
//
// Function Name : isObstacle
// Parameters : position<T> current_bot_position
// Return Type : bool
//---------------------------------------
template <class T>
bool Env_map<T>::isObstacle(position<T> current_bot_position){
    position<T> new_position = current_bot_position + bot_position;

    if(new_position.getX()>=0 && new_position.getX()<ROOM_ROWS &&
            new_position.getY()>=0 && new_position.getY()<ROOM_COLS){
        if(mapArray[new_position.getX()][new_position.getY()]==0){
            return false;
        }
        return true;
    }else{
        /*cout << "ERROR: Input position for isObstacle function of class Env_map is invalid, X : "
             <<current_bot_position.getX()<<" Y :"<<current_bot_position.getY() << std::endl;*/
        //exit(EXIT_FAILURE);
        return true;
    }
}

//-------------------------------------------------------------------------
// Returns the neighbors for a positon
// provided as input
//
// Function Name : getNeighbors
// Parameters : position<T> current_bot_position
// Return Type : neighbors (struct)
//
// Every bot would need to have its own map created
// When the map is initialized with bot position,
// all other neighbor calculations are done using the initial position
// information of the bot

//Anything 0 in the map array created corresponds to available space
//-------------------------------------------------------------------------
/*
template <class T>
neighbors Env_map<T>::getNeighbors(position<T> current_bot_position){
    neighbors _neighbors;

    //locations based on stored map
    T x_new = current_bot_position.x_position + get_position().x_position;
    T y_new = current_bot_position.y_position + get_position().y_position;
    cout<<"x_new : "<<x_new<<endl;
    cout<<"y_new : "<<y_new<<endl;

    //if x location is valid
    if(x_new>=0 && x_new<=ROOM_ROWS){
        if(x_new==0){
            _neighbors.top_neighbor_available = false;
            (mapArray[x_new+1][y_new]==0)?_neighbors.bottom_neighbor_available = true : _neighbors.bottom_neighbor_available = false;

        }
        else if(x_new==ROOM_ROWS-1){
            _neighbors.bottom_neighbor_available = false;
            (mapArray[x_new-1][y_new]==0)?_neighbors.top_neighbor_available = true : _neighbors.top_neighbor_available = false;

        }
        else{
            cout<<"coming to the x_new default"<<endl;
            cout<<"mapArray[x_new+1][y_new] :: "<<mapArray[x_new+1][y_new]<<endl;
            cout<<"mapArray[x_new-1][y_new] :: "<<mapArray[x_new-1][y_new]<<endl;
            cout<<"mapArray[x_new+1][y_new]==0)"<<(mapArray[x_new+1][y_new]==0)<<endl;
            (mapArray[x_new+1][y_new]==0)?_neighbors.bottom_neighbor_available = true : _neighbors.bottom_neighbor_available = false;
            (mapArray[x_new-1][y_new]==0)?_neighbors.top_neighbor_available = true : _neighbors.top_neighbor_available = false;


        }
    }else{
        _neighbors.invalid_input = true;
        cout<<"coming to the x_new else"<<endl;
    }

    //if y location is valid
    if(y_new>=0 && y_new<=ROOM_COLS){
        if(y_new==0){
            _neighbors.left_neighbor_available = false;
            (mapArray[x_new][y_new+1]==0)?_neighbors.right_neighbor_available = true : _neighbors.right_neighbor_available = false;
        }
        else if(y_new==ROOM_COLS-1){
            _neighbors.right_neighbor_available = false;
            (mapArray[x_new][y_new-1]==0)?_neighbors.left_neighbor_available = true : _neighbors.left_neighbor_available = false;
        }
        else{
            cout<<"coming to the y_new default"<<endl;
            cout<<"mapArray[x_new][y_new+1] :: "<<mapArray[x_new][y_new+1]<<endl;
            cout<<"mapArray[x_new][y_new-1] :: "<<mapArray[x_new][y_new-1]<<endl;
            (mapArray[x_new][y_new+1]==0)?_neighbors.right_neighbor_available = true : _neighbors.right_neighbor_available = false;
            (mapArray[x_new][y_new-1]==0)?_neighbors.left_neighbor_available = true : _neighbors.left_neighbor_available = false;

        }
    }else{
        _neighbors.invalid_input = true;
        cout<<"coming to the y_new default"<<endl;
    }

    return _neighbors;

}
*/
