//========================================================
// OnlineMap.h
//
// Author: Rishabh Gulati
//========================================================


#pragma once

#include <map>
#include <string.h>
#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <vector>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>

using namespace std;

# define LOGIC_DEBUG 0
# define COMM_DEBUG 0

template <class T, template <class> class N>
class OnlineMap
{
    int sockfd;
    //communication, serialization & deserialization methods
    string sendToServer(string message) const;
    recievedMsg deserializeMsgType(string SerializedString) const;
    vector<string> Tokenoize(string str)const;
    void connectInit(string ip, int port);
    //for message serialization & deserialization
    string delimiter;

public:

    OnlineMap (string ip, int port) { delimiter = "..";connectInit(ip,port);/*TODO server startup*/}
    void setMarked(N<T> position, int markAs) const;
    bool isPresent(N<T> position) const;
    bool isCleaned(N<T> position) const;

    /*end connection in destructor*/
    ~OnlineMap(){close(sockfd);}
};


//---------------------------------------
// Connects to the TCP server
//
// Function Name : connect
// Parameters : string ip, int port
// Return Type : void
//---------------------------------------
template <class T, template <class> class N>
void OnlineMap<T,N >::connectInit(string ip, int port)
{
    //int sockfd;
    struct sockaddr_in serv_addr;

    try{
        sockfd = socket(AF_INET, SOCK_STREAM, 0);

        memset(&serv_addr, 0, sizeof (serv_addr));
        serv_addr.sin_family = AF_INET;
        inet_aton(ip.c_str(), &serv_addr.sin_addr);
        serv_addr.sin_port = htons(port);

        connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr));

    }
    catch (exception& e)
    {
        cerr << "Exception caught in class 'OnlineMap', function 'sendToServer' : " << e.what() << '\n';
    }
}


//---------------------------------------
// Marks the position passed to this function
// in the map at the server
//
// Function Name : setMarked
// Parameters : N<T> position
// Return Type : void
//---------------------------------------
template <class T, template <class> class N>
void OnlineMap<T,N >::setMarked(N<T> position, int markAs) const
{
    recievedMsg receivedMsg;
    stringstream stream;
    stream << SetMarked_Request << delimiter << position.getX() << delimiter << position.getY() << delimiter << markAs;

    string receivedString = sendToServer(stream.str());
    receivedMsg = deserializeMsgType(receivedString);
    if (COMM_DEBUG){
        cout<<"type\n"<<receivedMsg.Msg_Type<<" ------- payload\n"<<receivedMsg.Serialized_payload<<endl;
    }
    if(receivedMsg.Msg_Type == SetMarked_Response){
        if (COMM_DEBUG){
            clog << "setMarked::Cell marked in map" << endl;
        }
    }else{
        throw invalid_argument ("ERROR: Message Response received for 'setMarked' function of class 'OnlineMap' is incorrect\n");
    }
}

//---------------------------------------
// TODO
//
// Function Name : isPresent
// Parameters : N<T> position
// Return Type : bool
//---------------------------------------
template <class T, template <class> class N>
bool OnlineMap<T,N >::isPresent(N<T> position)const
{
    recievedMsg receivedMsg;
    stringstream stream;
    try{
        stream << GetPosition_Request << delimiter << position.getX() << delimiter << position.getY();

        string receivedString = sendToServer(stream.str());
        receivedMsg = deserializeMsgType(receivedString);
        if(receivedMsg.Msg_Type == GetPosition_Response){
            if (COMM_DEBUG){
                clog << "isPresent::received correct response" << endl;
                clog << "value received : " << receivedMsg.Serialized_payload << endl;
            }
            switch(atoi(receivedMsg.Serialized_payload.c_str()))
            {
            case 0 :
            case 1 :
            case 2 : return true;
            default : return false;
            }
        }else{
            throw invalid_argument ("ERROR: Message Response received for 'isPresent' function of class 'OnlineMap' is incorrect\n");
        }
        return false;
    }catch (exception& e)
    {
        cerr << "Exception caught in class 'OnlineMap', function 'isPresent' : " << e.what() << '\n';
    }
}

//---------------------------------------
// Returns true if the position passed is
// cleaned and false otherwise
//
// Function Name : isCleaned
// Parameters : N<T> position
// Return Type : bool
//---------------------------------------
template <class T, template <class> class N>
bool OnlineMap<T,N >::isCleaned(N<T> position)const
{
    recievedMsg receivedMsg;
    stringstream stream;
    try{
        stream << GetPosition_Request << delimiter << position.getX() << delimiter << position.getY();

        string receivedString = sendToServer(stream.str());
        receivedMsg = deserializeMsgType(receivedString);
        if(receivedMsg.Msg_Type == GetPosition_Response){
            if (COMM_DEBUG){
                clog << "isCleaned::received correct response";
                clog << "..........value received : " << receivedMsg.Serialized_payload << endl;
            }

            switch(atoi(receivedMsg.Serialized_payload.c_str()))
            {

            case YetToBeCleaned : return false;
            case Cleaned : return true;
            case Obstacle : return true;
                //throw invalid_argument ("ERROR:In function 'isCleaned' position queried is an obstacle\n");
            default : return true;
                //throw invalid_argument ("ERROR:In function 'isCleaned' position queried returned a wrong value from the server\n");
            }
        }else{
            throw invalid_argument ("ERROR: Message Response received for 'isCleaned' function of class 'OnlineMap' is incorrect\n");
        }
        return true;
    }catch (exception& e)
    {
        cerr << "Exception caught in class 'OnlineMap', function 'isCleaned' : " << e.what() << '\n';
        //return false;
    }

}


//---------------------------------------
// Sends the message passed to the server
// and returns the response to the calling
// function
//
// Function Name : sendToServer
// Parameters : string message
// Return Type : string
//---------------------------------------
template <class T, template <class> class N>
string OnlineMap<T,N >::sendToServer(string message)const
{
    int n;
    char buffer[256];

    try{
        memset(buffer, 0, 256);
        int length = message.copy(buffer,message.length(),0);
        buffer[length]='\0';

        n = write(sockfd,buffer,length);
        memset(buffer, 0, 256);
        n = read(sockfd,buffer,255);
        //cout<<"received : "<<buffer<<endl;

    }
    catch (exception& e)
    {
        cerr << "Exception caught in class 'OnlineMap', function 'sendToServer' : " << e.what() << '\n';
    }
    string receivedData(buffer);
    return receivedData;
}


//---------------------------------------
// Returns a structure of type receivedMsg
// for further message processing
//
// Function Name : deserializeMsgType
// Parameters : string SerializedString
// Return Type : recievedMsg(struct)
//---------------------------------------
template <class T, template <class> class N>
recievedMsg OnlineMap<T,N >::deserializeMsgType(string SerializedString)const
{
    recievedMsg payload;
    vector<string> strvector;
    strvector=Tokenoize(SerializedString);
    vector<string>::iterator itertype=strvector.begin();
    payload.Msg_Type=atoi((const char*)(*itertype).c_str());
    payload.Serialized_payload=SerializedString.substr((*itertype).length()+delimiter.length(),SerializedString.length());
    return payload;
}


//---------------------------------------
// Returns tokenized vector of strings
//
// Function Name : Tokenoize
// Parameters : string str
// Return Type : vector<string>
//---------------------------------------
template <class T, template <class> class N>
vector<string> OnlineMap<T,N >::Tokenoize(string str)const
{
    vector<string> strvect;
    size_t pos=0, last_pos=0;
    while(last_pos != string::npos)
    {
        last_pos=str.find(delimiter,last_pos+1);
        if(last_pos != string::npos)
            strvect.push_back(str.substr(pos,(last_pos-pos)));
        else
            strvect.push_back(str.substr(pos,(str.size()-pos)));
        pos=delimiter.size()+last_pos;
    }
    return strvect;
}


