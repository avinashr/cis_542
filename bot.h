/*
template <class N>
void explore(Map<N,Set<N>> g)
// g is an undirected graph, i.e. a symmetric binary relation
// requires (\A N n,m: g[n][m] <==> g[m][n])
{
    Set<N> marked1;
    Stack<N> spine;
    <vacuum the root>;
    marked1[root] = true;
    spine.push(root);
    while (!spine.empty())
    // invariant: every node of G is marked1 or reachable by an unmarked1 path from a neighbor of the spine
    // invariant: every node of the spine is marked1, and the spine is a path of G (i.e., successive nodes are neighbors in G)
    // invariant: the spine is empty or its bottom node is the root (this guarantees that we will end up at the root)
    // decreases: 2 * (<# unmarked1 nodes of G>) - spine.length()
    {
        N n = spine.top();
        if (<n has an unmarked1 neighbor m>) {
            <move to m and vacuum m>;
            marked1[m] = true;
            spine.push(m);
        } else spine.pop();
    }
}
*/


//========================================================
// bot.h
// Defines Bot functionality
// Author: Avinash Repaka, Rishabh Gulati
//========================================================


#pragma once

#include <map>
#include <string.h>
#include <iostream>
#include <stdexcept>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/types.h>


#include "Env_map.h"
#include "stack.h"
#include "OnlineMap.h"

//using namespace std;

# define DEBUG 1

//N is position and T is int
template <class T, template <class> class N>
class bot
{
    //map<N<T>, CellState> marked1;
    stack< N<T> > spine;
    Env_map<T> envMap;
    N<T> initialPosition;
    bool updateNeighbors (N<T> n);
    OnlineMap<T,N> marked;

public:
    bot (string s, T X, T Y, N<T> init, string ip, int port) : envMap(s,X,Y), initialPosition(init), marked(ip,port) { envMap.initialize(initialPosition); }
    void explore (void);   
};

// This function updates the neighbors of a given position using environment map
// It updates whether the neighbor is an obstacle or yet to be cleaned if it hasn't been updated earlier
// It returns 'true' if at least one of the neighbors is yet to be cleaned
template <class T, template <class> class N>
bool bot<T,N >::updateNeighbors (N<T> n)
{
    // it's a good idea to
    bool flagToBeCleaned = false;

    for (uint8_t i=0; i<NoOfDirections; i++){
        N<T> neighbor = n.getNeighbor(Direction(i));

        if (LOGIC_DEBUG){
            //cout<<"NEIGHBOR avinash : "<<(marked1.find(neighbor)== marked1.end())<<"......rishabh : "<< !marked.isPresent(neighbor)<<endl;
        }
        //if(marked1.find(neighbor)== marked1.end()){
        if(!marked.isPresent(neighbor)){
            if (envMap.isObstacle(neighbor)) {
                //marked1[neighbor] = Obstacle;
                marked.setMarked(neighbor,Obstacle);
            }else{
                //marked1[neighbor] = YetToBeCleaned;
                marked.setMarked(neighbor,YetToBeCleaned);
                flagToBeCleaned = true;
            }
        }else{
            if (LOGIC_DEBUG){
                //cout<<"flagToBeCleaned avinash : "<<(marked1[neighbor] == YetToBeCleaned)<<"......rishabh : "<< !marked.isCleaned(neighbor)<<endl;
            }
            //if (marked1[neighbor] == YetToBeCleaned){
            if(!marked.isCleaned(neighbor)){
                flagToBeCleaned = true;
            }
        }
    }
    return flagToBeCleaned;
}

// This function defines how the bot moves and cleans
template <class T, template <class> class N>
void bot<T,N >::explore (void)
{
    N<T> root(0,0);

    //Vaccum the root position
    //marked1[root]=Cleaned;
    marked.setMarked(root,Cleaned);
    spine.push(root);
    updateNeighbors(root);

    while(!spine.empty())
    {
        N<T> n =spine.topNode();
        bool flagIsCleaned = true;

        for (uint8_t i=0; i<NoOfDirections; i++){
            N<T> m = n.getNeighbor(Direction(i));

            if (LOGIC_DEBUG){
                //cout<<"avinash : "<<!marked1[m]<<"......rishabh : "<< !marked.isCleaned(m)<<endl;
            }
            //if (!marked1[m]){
            if(!marked.isCleaned(m)){
                flagIsCleaned = false;
                //<go to m using shortest path algorith>
                //<vaccum m>
                //marked1[m] = Cleaned;
                marked.setMarked(m,Cleaned);
                if (DEBUG){
                    clog<<"Cell that is being cleaned : " <<m <<endl;
                }
                if(updateNeighbors(m)){
                    spine.push(m);
                    if (DEBUG){
                        clog<<"Cell that is being pushed : " <<m <<endl;
                    }
                }
                break;
            }
        }
        //sleep(1);
        //<if it is has unmarked1 neighbours, add it to spine>
        if (flagIsCleaned){
            N<T> c = spine.pop();
            if (DEBUG){
                clog << "Cell that is being popped out : " <<c <<endl;
            }
        }
    }
}
