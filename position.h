//========================================================
// position.h
// Defines class 'position'
// Author: Avinash Repaka
//========================================================

#pragma once

#include <stdexcept>

#include "common_utils.h"

using namespace std;

template <class T>
class position {
    T x;
    T y;
	// document your interface
public:
    position () {}
    position (T x0,T y0) : x(x0), y(y0) {}
    position<T>& operator= (const position<T>&);
	// addition of position would seem to be semantically meaninglesss
    position<T>& operator+=(const position<T>&);
    bool operator< (const position<T>&) const;
    position<T> getNeighbor(Direction) const;
    T getX (void) const;
    T getY (void) const;
};

template <class T>
position<T>& position<T>::operator=(const position<T>& rhs)
{
    x = (rhs.x);
    y = (rhs.y);
    return *this;
}

template <class T>
position<T>& position<T>::operator+=(const position<T>& rhs)
{
    x = x+rhs.x;
    y = y+rhs.y;
    return *this;
}

template <class T>
position<T> operator+(position<T> lhs, const position<T>& rhs)
{
    lhs += rhs;
    return lhs;
}

template <class T>
bool position<T>::operator< (const position<T>& rhs) const
{
    if ( x == rhs.x ){
        return y < rhs.y;
    }
    return x < rhs.x;
}

template <class T>
std::ostream& operator<< (std::ostream& os, const position<T>& obj)
{
    os << obj.getX() <<","<< obj.getY();
    return os;
}

template <class T>
position<T> position<T>::getNeighbor (Direction d) const
{
    switch(d)
    {
    case North: {position<T> p(x,y+1); return p;}
    case South: {position<T> p(x,y-1); return p;}
    case East: {position<T> p(x+1,y); return p;}
    case West: {position<T> p(x-1,y); return p;}
    default: throw invalid_argument ("ERROR: Input for 'getNeigbor' function of class 'position' is out of limits\n");
    }
}

template <class T>
T position<T>::getX (void) const
{
    return this->x;
}

template <class T>
T position<T>::getY (void) const
{
    return this->y;
}
